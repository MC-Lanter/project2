/**
 *  This class is the main class of the "World of Zuul" application. 
 *  "World of Zuul" is a very simple, text based adventure game.  Users 
 *  can walk around some scenery. That's all. It should really be extended 
 *  to make it more interesting!
 * 
 *  To play this game, create an instance of this class and call the "play"
 *  method.
 * 
 *  This main class creates and initialises all the others: it creates all
 *  rooms, creates the parser and starts the game.  It also evaluates and
 *  executes the commands that the parser returns.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2011.08.08
 * @conversion to C++ by Mikey Lanter
 */

#include <iostream>
#include <string>
#include <vector>
#include "Parser.h"
#include "Room.h"
#include "Game.h"

using namespace std;

	/**
	 * Create the game and initialise its internal map.
	 */
	Game::Game()
	{
		createRooms();
	}

	/**
	 * Create all the rooms and link their exits together
	 */
	void Game::createRooms()
	{
		Room *outside, *theater, *pub, *lab, *office;

		// create the rooms
        outside = new Room("outside the main entrance of the university");
        theater = new Room("in a lecture theater");
        pub = new Room("in the campus pub");
        lab = new Room("in a computing lab");
        office = new Room("in the computing admin office");

        // initialise room exits
        outside->setExit("east", theater);
        outside->setExit("south", lab);
        outside->setExit("west", pub);

        theater->setExit("west", outside);

        pub->setExit("east", outside);

        lab->setExit("north", outside);
        lab->setExit("east", office);

        office->setExit("west", lab);

        currentRoom = outside;
	}

	/**
	 * Main play routine. Loops until end of play
	 */
	void Game::play()
	{
		printWelcome();
		cout << endl;

        // Enter the main command loop.  Here we repeatedly read commands and
        // execute them until the game is over.

        bool done = false;
        while (!done) 
        {
            Command command = parser.getCommand();
            done = processCommand(command);
        }

        cout << "Thank you for playing.  Good bye." << endl;
    }

    /**
     * Print the opening message for the player.
     */
    void Game::printWelcome()
    {
    	cout << endl;
    	cout << "Welcome to the World of Zuul" << endl;
    	cout << "World of Zuul is a new, incredibly, astoundingly, magnificently boring adventure game." <<endl;
    	cout << "Type 'help' if you need help." << endl;
    	cout << endl;
    	cout << currentRoom->getLongDescription() << endl;
    }

    /**
     * Given a command, process (that is: execute) the command.
     * @param command The command to be processed.
     * @return true If the command ends the game, false otherwise.
     */
    bool Game::processCommand(Command command)
    {
        bool wantToQuit = false;

        if(command.isUnknown()) 
        {
            cout << "I don't know what you mean..." << endl;
            cout << endl;
            return false;
        }

        string commandWord = command.getCommandWord();

        if(commandWord == "help")
        	printHelp();
        else if(commandWord == "go")
        	goRoom(command);
        else if(commandWord == "quit")
        	wantToQuit = quit(command);

        //else command is unrecognised
        return wantToQuit;
    }    

    /**
     * Print out some help information / such as the commands available.
     * Here we print stupid, bullshit words we make with our mouths.
     */
    void Game::printHelp()
    {
    	cout << "You are lost. You are alone. You wander" << endl;
    	cout << "around at the university, racking up student debt." << endl;
    	cout << endl;
    	cout << "Your command words are: " << endl;
    	parser.showCommands();
    }

    /**
     * Try to in to one direction. If there is an exit, enter the new
     * room, otherwise print an error message.
     */
    void Game::goRoom(Command command)
    {
    	string direction = command.getSecondWord();

    	if(command.hasSecondWord())
    	{
    		Room *nextRoom = currentRoom->getExit(direction);

    		if(nextRoom == NULL)
    		{
    			cout << "You can't go this way!" << endl;   		
    		}
    		else
    		{
    			currentRoom = nextRoom;
    			cout << nextRoom->getLongDescription() <<endl;
    			cout << endl;
    		}
    	}
    	else
    	{
    		cout << "Go Where?" << endl;
    		return;
    	}
    }

    /**
     * "Quit" was entered. Check the rest of the command to see
     * whether we really quit the game.
     * @return true, if this command quits the game, false otherwise.
     */
    bool Game::quit(Command command)
    {
        if(command.hasSecondWord()) 
        {
            cout << "Quit what?" << endl;
            return false;
        }

        else 
        {
            return true;  // signal that we want to quit
        }
    }








