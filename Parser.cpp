/**
 * This class is part of the "World of Zuul" application. 
 * "World of Zuul" is a very simple, text based adventure game.  
 * 
 * This parser reads user input and tries to interpret it as an "Adventure"
 * command. Every time it is called it reads a line from the terminal and
 * tries to interpret the line as a two word command. It returns the command
 * as an object of class Command.
 *
 * The parser has a set of known command words. It checks user input against
 * the known commands, and if the input is not one of the known commands, it
 * returns a command object that is marked as an unknown command.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2011.08.08
 * @conversion to C++ by Mikey Lanter
 */

#include <iostream>
#include <string>
#include <vector>
#include "CommandWords.h"
#include "Command.h"
#include "Parser.h"

using namespace std;

	/**
     * Create a parser to read from the terminal window.
     */
	Parser::Parser()
	{		
	}

	Command Parser::getCommand()
	{
		string inputLine;
		string word1 = "";
		string word2 = "";

		cout << "input command> ";
		//cin >> inputLine; (do not use as it only stores first "chunk")
		getline(cin, inputLine);

		word1 = getWord(inputLine);

		int subFirstWord = word1.length();
		int inputLength = inputLine.length();
		inputLine = inputLine.substr(subFirstWord, inputLength);

		word2 = getWord(inputLine);

		if(commands.isCommand(word1))
		{
			return *(new Command(word1, word2));
		}
		else
		{
			return *(new Command("", word2));
		}	
	}

	/**
	* Removes spaces so we can get the word
	*/
	string Parser::getWord(string &inputLine)
	{
		int spaceRemover;
		bool done = false;
		while(done != true)
		{
			spaceRemover = inputLine.find_first_of(" ");
			if(spaceRemover == 0)
			{
				inputLine = inputLine.substr(1, inputLine.length());
			}
			else
			{
				done = true;
			}		
		}
		return inputLine.substr(0, spaceRemover);	
	}

	void Parser::showCommands()
    {
      commands.showAll();
    }