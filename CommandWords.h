/**
 * This class is part of the "World of Zuul" application. 
 * "World of Zuul" is a very simple, text based adventure game.
 * 
 * This class holds an enumeration of all command words known to the game.
 * It is used to recognise commands as they are typed in.
 *
 * @author  Michael Kölling and David J. Barnes
 * @version 2011.08.08
 * @conversion to C++ by Mikey Lanter	
 */

 #ifndef COMMANDWORDS_H
 #define COMMANDWORDS_H

 #include <iostream>
 #include <string>
 #include <vector>

 using namespace std;

 class CommandWords
 {
 	private:
 		//vector containing all valid command words ("go" "help" "quit")
 		vector<string> validCommands;

 	public:
 		/**
 		* The constructor for CommandWords class with no parameters
 		*/
 		CommandWords(); 

 		/**
     	* Check whether a given String is a valid command word. 
     	* @return true if it is, false if it isn't.
     	*/
 		bool isCommand(string aString);

 		/**
     	* Print all valid commands to System.out.
     	*/
 		void showAll();
 };
 #endif