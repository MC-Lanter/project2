/**
 * This class is part of the "World of Zuul" application. 
 * "World of Zuul" is a very simple, text based adventure game.
 * 
 * This class holds an enumeration of all command words known to the game.
 * It is used to recognise commands as they are typed in.
 *
 * @author  Michael Kölling and David J. Barnes
 * @version 2011.08.08
 * @conversion to C++ by Mikey Lanter	
 */

 #include <iostream>
 #include <string>
 #include <vector>
 #include <iterator>
 #include "CommandWords.h"

 using namespace std;


 	/**
 	* The constructor for CommandWords class with no parameters
 	*/
 	CommandWords::CommandWords()
 	{
 		validCommands.push_back("go");
 		validCommands.push_back("help");
 		validCommands.push_back("quit");
 	}

	/**
  	* Check whether a given String is a valid command word. 
   	* @return true if it is, false if it isn't.
   	*/
 	bool CommandWords::isCommand(string aString)
 	{
 		for(vector<string>::iterator it = validCommands.begin();
 			it != validCommands.end(); it++)
 		{
 			if(*it == aString)
 				return true;

 		}
 		return false;
 	}

	/**
     * Print all valid commands to System.out.
     */
 	void CommandWords::showAll()
 	{
 		for(vector<string>::iterator it = validCommands.begin();
 			it != validCommands.end(); it++)
 		{
 			cout << *it << endl;
 		}
 	}